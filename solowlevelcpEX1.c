#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<errno.h>
#include<stdlib.h>

int BUFFSIZE = 128;

void ioCopy(int IN,int OUT);

int main(int argc, char *argv[])
{
  int fdIn = open( argv[1], O_RDONLY);
  if (fdIn < 0)
    perror("Erro na abertura do ficheiro!\n");
  int fdOut = creat( argv[2], S_IRUSR| S_IWUSR);
  if (fdOut < 0)
    perror("Erro na criação do ficheiro!\n");
  BUFFSIZE = atoi(argv[3]);
  ioCopy(fdIn, fdOut);
  return 0;
}

void ioCopy(int IN,int OUT)//no error reporting
{
    int n;
    char *buf = malloc(BUFFSIZE);
    while((n =read (IN, buf, BUFFSIZE)) > 0)
    {
        if(write (OUT,buf,n)!=n)
            perror("Erro de escrita!\n");
    }
    if(n < 0)
        perror("Erro de leitura!\n");
}
