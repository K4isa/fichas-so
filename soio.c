/*
 *  soio.c
 *
 *  Created by Paul Crocker on 3/11/07.
 *  Copyright 2007 UBI. All rights reserved.
 *
 */
#include "soio.h"

SOFILE *sofopen (const char *nome, const char *mode){
  SOFILE *novo = NULL;
  int fd;

  if (mode[0] == 'r')
    fd = open (nome, O_RDONLY, 0);
  else
    exit (1);            //agora apenas para leitura !

  if (-1 == fd)
    return NULL;        //caso erro

  novo = (SOFILE *) malloc (sizeof (SOFILE));
  novo->buf = (char *) malloc (MAXBUFFSIZE);
  novo->fd = fd;
  novo->index = 0;
  novo->size = 0;
  return novo;}

//Funções não implementadas

int sofclose (SOFILE * fp){
  /* return 0; */
  close(fp->fd);
  if( fp == NULL) return -1;
  free(fp->buf);
  fp->index = 0;
  fp->fd = 0;
  fp->size = 0;
  free(fp);
  return 0;}

int sofgetc (SOFILE * fp){
  /*substituir este codigo 
  int x[] = { 'x', 'o', 'l', 'a', '\n', -1 };
  static int i = 0;
  return x[i++];
  */
  int n = 0;
  if (fp->index){
    fp->index = 0;
    return fp->index;}
  n = read(fp->fd, fp->buf, 1);
  if (n <= 0)
    return (-1);
  return fp->buf;}

int sofflush (SOFILE * fp){
  int ch;
  do {
    ch = fgetc(fp);} 
  while (ch != '\n' && ch != EOF);
  if (ch == EOF && ferror(fp)){
    return -1;}
  return 0;}